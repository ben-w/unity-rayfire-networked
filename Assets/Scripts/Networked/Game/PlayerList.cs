using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerList : MonoBehaviour
{
    [SerializeField] private GameObject _prefab;
    [SerializeField] private Dictionary<ushort, PlayerListItem> _playerDictionary;

    private void Awake()
    {
        _playerDictionary = new Dictionary<ushort, PlayerListItem>();
        NetworkManager.PlayerJoined += PlayerJoined;
        NetworkManager.PlayerDisconnected += PlayerDisconnected;
    }

    private void PlayerDisconnected(PlayerManager playerInstance)
    {
        if (_playerDictionary.TryGetValue(playerInstance.OwnerID, out PlayerListItem player))
        {
            _playerDictionary.Remove(playerInstance.OwnerID);
            if (player != null)
                Destroy(player.gameObject);
            Debug.Log("Removed Player");
            return;
        }
        Debug.LogError("Failed to remove player. Player not in our list");
    }

    private void PlayerJoined(PlayerManager player)
    {
        GameObject newItem = Instantiate(_prefab, transform);
        PlayerListItem listItem = newItem.GetComponent<PlayerListItem>();

        _playerDictionary.Add(player.OwnerID, listItem);

        listItem.Setup($"User {player.OwnerID} ", 0);
        Debug.Log($"Added player to player list");
    }
}