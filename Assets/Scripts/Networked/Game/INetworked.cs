﻿using DarkRift;

interface INetworked
{
    public ushort UID { get; set; }
    public void MessageReceived(DarkRiftReader reader);
}