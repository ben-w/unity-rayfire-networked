﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public static class Debug
{

    public static void Log(object message, UnityEngine.Object context = null)
    {
        var stackTrace = new StackTrace().GetFrame(1);
        var caller = stackTrace.GetMethod().Name;
        var go = context == null ? string.Empty : $"{context} : ";
        var hue = (caller.GetHashCode() / (float)int.MaxValue) * 0.5f + 0.5f;
        var callerColour = Color.HSVToRGB(hue, .6f, .8f, false).Hex();
        UnityEngine.Debug.Log($"<color={callerColour}><b>[{go}{caller}]</b></color>{message}", context);
    }

    public static void LogWarning(object message, UnityEngine.Object context = null)
    {
        var stackTrace = new StackTrace().GetFrame(1);
        var caller = stackTrace.GetMethod().Name;
        var go = context == null ? string.Empty : $"{context} : ";
        var hue = (caller.GetHashCode() / (float)int.MaxValue) * 0.5f + 0.5f;
        var callerColour = Color.HSVToRGB(hue, .6f, .8f, false).Hex();
        UnityEngine.Debug.LogWarning($"<color={callerColour}><b>[{go}{caller}]</b></color>{message}", context);
    }

    public static void LogError(object message, UnityEngine.Object context = null)
    {
        var stackTrace = new StackTrace().GetFrame(1);
        var caller = stackTrace.GetMethod().Name;
        var go = context == null ? string.Empty : $"{context} : ";
        var hue = (caller.GetHashCode() / (float)int.MaxValue) * 0.5f + 0.5f;
        var callerColour = Color.HSVToRGB(hue, .6f, .8f, false).Hex();
        UnityEngine.Debug.LogError($"<color={callerColour}><b>[{go}{caller}]</b></color>{message}", context);
    }

    public static string Hex(this Color color)
    {
        return "#" + ToHtmlStringRGB(color);
    }

    /// <summary>
    ///   <para>Returns the color as a hexadecimal string in the format "RRGGBB".</para>
    /// </summary>
    /// <param name="color">The color to be converted.</param>
    /// <returns>
    ///   <para>Hexadecimal string representing the color.</para>
    /// </returns>
    public static string ToHtmlStringRGB(Color color)
    {
        Color32 color32 = new Color32((byte)Mathf.Clamp(Mathf.RoundToInt(color.r * byte.MaxValue), 0, byte.MaxValue), (byte)Mathf.Clamp(Mathf.RoundToInt(color.g * byte.MaxValue), 0, byte.MaxValue), (byte)Mathf.Clamp(Mathf.RoundToInt(color.b * byte.MaxValue), 0, byte.MaxValue), 1);
        return Format("{0:X2}{1:X2}{2:X2}", color32.r, color32.g, color32.b);
    }

    public static string Format(string fmt, params object[] args) => string.Format(CultureInfo.InvariantCulture.NumberFormat, fmt, args);

}
#endif