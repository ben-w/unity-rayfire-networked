using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DarkRift;
using DarkRift.Server.Unity;
using DarkRift.Client.Unity;
using DarkRift.Client;
using System;
using DarkRift.Server;

class NetworkManager : MonoBehaviour
{
    public static Action<PlayerManager> PlayerJoined;
    public static Action<PlayerManager> PlayerDisconnected;

    public static bool IsServer = false;
    public static UnityClient Client { get => _instance._client; }
    private static NetworkManager _instance;

    public bool EnableDebugMessages = true;
    public List<GameObject> NetworkedPrefabs = new List<GameObject>();

    [SerializeField] private GameObject _playerPrefab;
    private UnityClient _client;

    #region Server Variables
    [Header("Server Variables")]
    private const string _spawnPointTag = "Respawn";
    private const string _nodeSpawnPointTag = "Node Spawn";
    private const ushort _clientID = ushort.MaxValue;

    [SerializeField] private float _nodeSpawnTimer = 1f;
    [SerializeField] private List<Transform> _nodeSpawns;
    [SerializeField] private bool _nodeTimerRunning = true;
    [SerializeField] private int _nodeTarget = 3;
    [SerializeField] private GameObject _nodePrefab;
    private WaitForSeconds _nodeDelay;
    private List<IClient> _clients = new List<IClient>();
    private GameObject[] _spawnPoints;
    private ushort _nextUID;

    #endregion

    public static string ConnectIP = "127.0.0.1";

    public static Dictionary<ushort, NetworkedBehaviour> NetworkedObjects = new Dictionary<ushort, NetworkedBehaviour>();
    public static Dictionary<ushort, PlayerManager> PlayersManagers = new Dictionary<ushort, PlayerManager>();

    private void Start()
    {
        _instance = this;
        DontDestroyOnLoad(this);
        SceneManager.sceneLoaded += SceneLoaded;

        Gather1.Server.MasterPlugin.ClientConnected += ServerClientConnected;
        Gather1.Server.MasterPlugin.ClientDisconnected += ServerClientDisconnected;
    }

    private void SceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        if (arg0.buildIndex != 1)
            return;

        GameObject[] objects = GameObject.FindGameObjectsWithTag("Manager");

        if (IsServer)
        {
            for (int i = 0; i < objects.Length; i++)
            {
                if (objects[i].name == "Server Manager")
                {
                    ServerFindSpawnPoints();
                    ServerFindNodeSpawnPoints();
                    XmlUnityServer server = objects[i].GetComponent<XmlUnityServer>();
                    server.Create();
                    Debug.Log("Server Started");

                    ServerSpawnPlayer(ServerGetRandomSpawnPoint(), ServerGetUID(),
                        _playerPrefab.name, _clientID);
                    _nodeDelay = new WaitForSeconds(_nodeSpawnTimer);
                    StartCoroutine(ServerNodeSpawnTimer());
                    return;
                }
            }
        }

        for (int i = 0; i < objects.Length; i++)
        {
            if (objects[i].name == "Client Manager")
            {
                _client = objects[i].GetComponent<UnityClient>();
                _client.MessageReceived += ClientMessageReceived;
                _client.Connect(ConnectIP, 4296, false);
                ClientRequestPlayer();
                Debug.Log("Connect Finished");
            }
        }
    }

    #region Server Methods

    public static void ServerSendMessageToAll(Message message, SendMode sendmode)
    {
        for (int i = 0; i < _instance._clients.Count; i++)
        {
            _instance._clients[i].SendMessage(message, sendmode);
        }
    }

    public static void ServerSendMessageToAll(NetworkTags tag, DarkRiftWriter writer, SendMode sendMode)
    {
        using (Message message = Message.Create((ushort)tag, writer))
        {
            ServerSendMessageToAll(message, sendMode);
        }
    }

    private void ServerClientConnected(object sender, ClientConnectedEventArgs args)
    {
        args.Client.MessageReceived += ServerClientMessageReceived;
        _clients.Add(args.Client);
    }

    private void ServerClientDisconnected(object sender, ClientDisconnectedEventArgs args)
    {
        Debug.Log("Removing Player");
        _clients.Remove(args.Client);

        RemovePlayer(args.Client.ID);

        using (DarkRiftWriter writer = DarkRiftWriter.Create())
        {
            writer.Write(args.Client.ID);
            using (Message message = Message.Create((ushort)NetworkTags.PlayerDisconnected, writer))
            {
                ServerSendMessageToAll(message, SendMode.Reliable);
            }
        }
    }

    private void ServerClientMessageReceived(object sender, DarkRift.Server.MessageReceivedEventArgs args)
    {
        using (Message message = args.GetMessage())
        using (DarkRiftReader reader = message.GetReader())
        {
            switch (message.Tag)
            {
                case (ushort)NetworkTags.RequestPlayer:
                    ServerRequestPlayer(reader, args);
                    break;
                case (ushort)NetworkTags.MoveObject:
                case (ushort)NetworkTags.ToolHide:
                case (ushort)NetworkTags.ToolSwitch:
                case (ushort)NetworkTags.HitNodeRequest:
                    ushort uid = reader.ReadUInt16();
                    // Validate?

                    if (NetworkedObjects.TryGetValue(uid, out NetworkedBehaviour behaviour))
                    {
                        behaviour.MessageReceived.Invoke(message, reader);
                    }
                    break;
            }
        }
    }

    private void ServerRequestPlayer(DarkRiftReader reader, DarkRift.Server.MessageReceivedEventArgs args)
    {
        // Validate stuff
        // ToDo: Get Client ID from server not client

        ServerSendExistingObjects(args.Client);

        ushort clientID = reader.ReadUInt16();
        string prefabName = reader.ReadString();
        Transform spawn = ServerGetRandomSpawnPoint();
        ushort uid = ServerGetUID();

        ServerSpawnPlayer(spawn, uid, prefabName, clientID);

        using (DarkRiftWriter writer = DarkRiftWriter.Create())
        {

            writer.Write(prefabName);
            writer.Write(uid);

            writer.Write(spawn.position.x);
            writer.Write(spawn.position.y);
            writer.Write(spawn.position.z);

            writer.Write(spawn.rotation.eulerAngles.x);
            writer.Write(spawn.rotation.eulerAngles.y);
            writer.Write(spawn.rotation.eulerAngles.z);

            writer.Write(clientID);

            using (Message message = Message.Create((ushort)NetworkTags.SpawnPrefab, writer))
            {
                ServerSendMessageToAll(message, SendMode.Reliable);
            }
        }

    }

    private void ServerFindSpawnPoints()
    {
        _spawnPoints = GameObject.FindGameObjectsWithTag(_spawnPointTag);
        Debug.Log($"Found {_spawnPoints.Length} spawn points");
    }

    private Transform ServerGetRandomSpawnPoint()
    {
        return _spawnPoints[UnityEngine.Random.Range(0, _spawnPoints.Length - 1)].transform;
    }

    public static ushort ServerGetUID()
    {
        ushort id = _instance._nextUID;
        _instance._nextUID++;
        return id;
    }

    private bool ServerSpawnPrefab(string prefabName, ushort uid,
        out GameObject networkObject, out NetworkedBehaviour behaviour)
    {
        GameObject prefab = GetNetworkPrefab(prefabName);
        if (prefab == null)
        {
            networkObject = null;
            behaviour = null;
            return false;
        }

        networkObject = Instantiate(prefab);
        behaviour = networkObject.GetComponent<NetworkedBehaviour>();
        behaviour.UID = uid;
        NetworkedObjects.Add(uid, behaviour);
        return true;
    }

    private void ServerSpawnPlayer(Transform spawn, ushort uid, string prefabName, ushort clientID)
    {
        bool suscessful = ServerSpawnPrefab(prefabName, uid,
            out GameObject networkObject, out NetworkedBehaviour behaviour);

        if (!suscessful)
            return;

        networkObject.transform.position = spawn.position;
        networkObject.transform.rotation = Quaternion.Euler(spawn.eulerAngles);


        PlayerManager player = behaviour as PlayerManager;
        player.Prefab = GetNetworkPrefab(prefabName);
        player.OwnerID = clientID;
        player.IsOwner = _clientID == clientID;
        networkObject.name = $"({clientID}) Player";
        player.EnableCompoents();
        PlayersManagers.Add(clientID, player);
    }

    private void ServerSendExistingObjects(IClient client)
    {
        foreach (NetworkedBehaviour item in NetworkedObjects.Values)
        {
            if (item.Prefab == null)
                continue;

            item.InitilizeData(out Message message, out DarkRiftWriter writer);
            client.SendMessage(message, SendMode.Reliable);
            message.Dispose();
            writer.Dispose();
        }
    }

    private void ServerFindNodeSpawnPoints()
    {
        GameObject[] gameObjects = GameObject.FindGameObjectsWithTag(_nodeSpawnPointTag);
        _nodeSpawns = new List<Transform>(gameObjects.Length);

        Debug.Log("Found " + gameObjects.Length);

        for (int i = 0; i < gameObjects.Length; i++)
        {
            Debug.Log($"{i} {gameObjects.Length} {_nodeSpawns.Count}");
            _nodeSpawns.Add(gameObjects[i].transform);
        }
    }

    private Transform ServerFindRandomNodeSpawn()
    {
        Transform spawn = _nodeSpawns[UnityEngine.Random.Range(0, _nodeSpawns.Count - 1)];
        int maxTries = 10;
        int currentTry = 0;

        while (spawn.childCount != 0 && currentTry < maxTries)
        {
            spawn = _nodeSpawns[UnityEngine.Random.Range(0, _nodeSpawns.Count - 1)];
            currentTry++;
        }

        return spawn;
    }

    private IEnumerator ServerNodeSpawnTimer()
    {
        while (_nodeTimerRunning)
        {
            ServerSpawnNodes();
            yield return _nodeDelay;
        }
    }

    private void ServerSpawnNodes()
    {
        int nodes = 0;

        for (int i = 0; i < _nodeSpawns.Count; i++)
        {
            if (_nodeSpawns[i].childCount > 0)
                nodes++;
        }

        if (nodes >= _nodeTarget)
            return;

        ServerSpawnNetworkedObject(_nodePrefab, Vector3.zero, new Vector3(90, 348.4829f), ServerFindRandomNodeSpawn());
    }

    private GameObject ServerSpawnNetworkedObject(GameObject prefab, Vector3 position, Vector3 rotation, Transform parent = null)
    {
        GameObject gameObject = Instantiate(prefab, parent);
        gameObject.transform.localPosition = position;
        gameObject.transform.localRotation = Quaternion.Euler(rotation);
        NetworkedBehaviour behaviour = gameObject.GetComponent<NetworkedBehaviour>();

        if (behaviour == null)
        {
            Debug.LogError($"{prefab.name} doesn't have NetworkedBehaviour");
            return null;
        }

        ushort uid = ServerGetUID();
        NetworkedObjects.Add(uid, behaviour);
        behaviour.UID = uid;
        behaviour.Prefab = prefab;


        behaviour.InitilizeData(out Message message, out DarkRiftWriter writer);

        ServerSendMessageToAll(message, SendMode.Reliable);

        message.Dispose();
        writer.Dispose();

        return gameObject;
    }

    #endregion

    #region Client Methods

    private void ClientRequestPlayer()
    {
        using (DarkRiftWriter writer = DarkRiftWriter.Create())
        {
            writer.Write(Client.ID);
            writer.Write(_playerPrefab.name);
            SendMessage(NetworkTags.RequestPlayer, writer, SendMode.Reliable);
        }
    }

    private void ClientMessageReceived(object sender, DarkRift.Client.MessageReceivedEventArgs args)
    {
        if (EnableDebugMessages)
            Debug.Log($"Received Message {args.Tag}");

        using (Message message = args.GetMessage())
        using (DarkRiftReader reader = message.GetReader())
        {
            switch (message.Tag)
            {
                case (ushort)NetworkTags.SpawnPrefab:
                    ClientSpawnPrefab(reader);
                    break;
                case (ushort)NetworkTags.PlayerDisconnected:
                    ClientPlayerDisconnected(reader);
                    break;
                case (ushort)NetworkTags.MoveObject:
                case (ushort)NetworkTags.ToolHide:
                case (ushort)NetworkTags.ToolSwitch:
                case (ushort)NetworkTags.HitNode:
                    ushort uid = reader.ReadUInt16();
                    if (NetworkedObjects.TryGetValue(uid, out NetworkedBehaviour behaviour))
                    {
                        behaviour.MessageReceived.Invoke(message, reader);
                    }
                    break;
            }
        }
    }

    private void ClientSpawnPrefab(DarkRiftReader reader)
    {
        string prefabName = reader.ReadString();
        GameObject prefab = GetNetworkPrefab(prefabName);

        if (prefab == null)
            return;

        GameObject networkObject = Instantiate(prefab);
        NetworkedBehaviour behaviour = networkObject.GetComponent<NetworkedBehaviour>();
        behaviour.Initialize(reader);
    }

    private void ClientPlayerDisconnected(DarkRiftReader reader)
    {
        ushort clientID = reader.ReadUInt16();
        RemovePlayer(clientID);
    }

    #endregion

    #region Helper Methods

    private GameObject GetNetworkPrefab(string prefabName)
    {
        GameObject prefab = null;

        for (int i = 0; i < NetworkedPrefabs.Count; i++)
        {
            if (NetworkedPrefabs[i].name == prefabName)
                prefab = NetworkedPrefabs[i];
        }

        if (prefab == null)
        {
            Debug.LogError($"Couldn't find \"{prefabName}\" in NetworkedPrefabs");
        }

        return prefab;
    }

    private void RemovePlayer(ushort id)
    {
        if (PlayersManagers.TryGetValue(id, out PlayerManager manager))
        {
            Destroy(manager.gameObject);
            PlayersManagers.Remove(id);
        }
        else
            Debug.LogError("Tried to remove player but not in our list");
    }

    public static void SendMessage(NetworkTags tag, DarkRiftWriter writer, SendMode sendMode)
    {
        using (Message message = Message.Create((ushort)tag, writer))
        {
            if (IsServer)
                ServerSendMessageToAll(message, sendMode);
            else
                _instance._client.SendMessage(message, sendMode);
        }
    }



    #endregion
}
