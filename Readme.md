# Unity Rayfire Networked
This is a prototype testing if it was feasible to network the destructions of meshes using Rayfire and Darkrift Networking 2.

## Warning, this project will break if you don't own the paid assets.
I have no included the paid assets in this repository. This is mainly just to read through the code and see how I did it.

Required Assets
- [Rayfire for Unity](https://assetstore.unity.com/packages/tools/game-toolkits/rayfire-for-unity-148690)