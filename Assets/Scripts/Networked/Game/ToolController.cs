using System;
using System.Collections;
using System.Collections.Generic;
using DarkRift;
using UnityEngine;
using RayFire;

class ToolController : MonoBehaviour
{
    public enum SlotType { Axe, Pickaxe }

    [SerializeField] private Transform _hand;
    [SerializeField] private Slot[] _slotObjects;
    [SerializeField] private PlayerManager _player;
    [SerializeField] private Camera _camera;
    [SerializeField] private PhysicMaterial _physicMaterial;
    [SerializeField] private float _playerReach = 5f;

    private int _activeSlot = 1;

    private void OnEnable() => _player.MessageReceived += MessageReceived;

    private void OnDisable() => _player.MessageReceived -= MessageReceived;

    private void MessageReceived(Message message, DarkRiftReader reader)
    {
        switch (message.Tag)
        {
            case (ushort)NetworkTags.ToolSwitch:
                SwitchSlot(reader.ReadInt32());
                break;
            case (ushort)NetworkTags.ToolHide:
                HideTool();
                break;
        }
    }

    private void Update()
    {
        if (!_player.IsOwner)
            return;
        Inputs();
    }

    private void Inputs()
    {
        if (Input.GetKeyDown(KeyCode.X))
            HideTool();

        if (Input.GetKeyDown(KeyCode.Alpha1))
            SwitchSlot(1);

        if (Input.GetKeyDown(KeyCode.Alpha2))
            SwitchSlot(2);

        if (Input.GetKeyDown(KeyCode.Alpha3))
            SwitchSlot(3);

        if (Input.GetMouseButtonDown(0))
            Swing();
    }

    private void SwitchSlot(int slot)
    {
        HideTool();
        _slotObjects[slot - 1].GameObject.SetActive(true);
        _activeSlot = slot;

        if (!_player.IsOwner)
            return;

        using (DarkRiftWriter writer = DarkRiftWriter.Create())
        {
            writer.Write(_player.UID);
            writer.Write(slot);
            NetworkManager.SendMessage(NetworkTags.ToolSwitch, writer, SendMode.Reliable);
        }
    }

    private void HideTool()
    {
        for (int i = 0; i < _slotObjects.Length; i++)
        {
            if (_slotObjects[i].GameObject != null)
                _slotObjects[i].GameObject.SetActive(false);
        }

        if (!_player.IsOwner)
            return;

        using (DarkRiftWriter writer = DarkRiftWriter.Create())
        {
            writer.Write(_player.UID);
            NetworkManager.SendMessage(NetworkTags.ToolHide, writer, SendMode.Reliable);
        }
    }

    private void Swing()
    {
        if (Physics.Raycast(_camera.transform.position,
            _camera.transform.forward, out RaycastHit hitInfo))
        {
            if (hitInfo.distance > _playerReach)
                return;

            Debug.Log($"Hit {hitInfo.transform.name}");
            if (hitInfo.transform.CompareTag("Node") &&
                _slotObjects[_activeSlot - 1].SlotType == SlotType.Pickaxe)
            {
                HitNode(hitInfo);
                return;
            }
        }
    }

    private void HitNode(RaycastHit hitInfo)
    {
        RayfireShatter shatter = hitInfo.transform.GetComponent<RayfireShatter>();
        if (shatter)
        {
            if (shatter.fragmentsAll.Count > 0)
                return;

            Node node = hitInfo.transform.GetComponent<Node>();

            if (!node)
            {
                Debug.LogError("Node was null");
                return;
            }

            Vector3 localPosition = node.transform.InverseTransformPoint(hitInfo.point);
            node.RequestHit(10, localPosition);

            Debug.Log("Found shatter");
        }
    }

    [Serializable]
    private struct Slot
    {
        public GameObject GameObject;
        public SlotType SlotType;
    }


}