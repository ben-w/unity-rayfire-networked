using System;
using System.Collections;
using System.Collections.Generic;
using DarkRift;
using UnityEngine;
using RayFire;

class Node : NetworkedBehaviour
{
    [SerializeField] private float _startingHealth = 100;
    [SerializeField] private float _health = 100;
    [SerializeField] private PhysicMaterial _rockMaterial;
    [SerializeField] RayfireShatter _rayfire;
    [SerializeField] private float _size;
    public float SellPrice;

    private bool _meshBroken = false;
    [SerializeField] private bool _breakAble = true;

    public override void OnEnable()
    {
        base.OnEnable();
        MessageReceived += NewMessage;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        MessageReceived -= NewMessage;
    }

    [ContextMenu("Client Hit 50")]
    private void ClientHit50() => RequestHit(50, Vector3.zero);

    [ContextMenu("Client Hit 100")]
    private void ClientHit100() => RequestHit(100, Vector3.zero);

    public void RequestHit(float damage, Vector3 hitPosition)
    {
        if (!_breakAble)
        {
            Debug.Log("This rock is not breakable");
            return;
        }

        if (NetworkManager.IsServer)
        {
            ServerHit(damage, hitPosition);
            return;
        }

        using (DarkRiftWriter writer = DarkRiftWriter.Create())
        {
            writer.Write(UID);
            writer.Write(damage);
            writer.Write(hitPosition.x);
            writer.Write(hitPosition.y);
            writer.Write(hitPosition.z);
            NetworkManager.SendMessage(NetworkTags.HitNodeRequest, writer, SendMode.Reliable);
        }
    }

    private void NewMessage(Message message, DarkRiftReader reader)
    {
        switch (message.Tag)
        {
            case (ushort)NetworkTags.HitNodeRequest:
                ServerHit(reader);
                break;
            case (ushort)NetworkTags.HitNode:
                ClientDamageNode(reader);
                break;
            default:
                break;
        }
    }

    private void ServerHit(DarkRiftReader reader)
    {
        if (!NetworkManager.IsServer)
            return;

        float damage = reader.ReadSingle();
        Vector3 position = new Vector3()
        {
            x = reader.ReadSingle(),
            y = reader.ReadSingle(),
            z = reader.ReadSingle()
        };

        // Validate

        ServerHit(damage, position);
    }

    private void ServerHit(float damage, Vector3 hitPosition)
    {
        if (!_breakAble)
        {
            Debug.LogWarning("Received invalid request to hit a rock which is unbreakable");
            return;
        }

        _health -= damage;
        Debug.Log("Received Damage Node. New Heath = " + _health);
        List<ushort> uids = null;
        if (_health <= 0)
        {
            _rayfire.centerPosition = hitPosition;
            _rayfire.Fragment();
            _rayfire.gameObject.SetActive(false);

            uids = new List<ushort>(_rayfire.fragmentsLast.Count);
            for (int i = 0; i < _rayfire.fragmentsLast.Count; i++)
            {
                uids.Add(NetworkManager.ServerGetUID());

                Node section = _rayfire.fragmentsLast[i].GetComponent<Node>();
                section.UID = uids[i];
                section._health = _startingHealth;
                section.Sync = Sync;

                MeshCollider meshCollider = _rayfire.fragmentsLast[i].GetComponent<MeshCollider>();
                if (meshCollider.bounds.size.magnitude >= 1)
                {
                    section._rayfire = _rayfire.fragmentsLast[i].AddComponent<RayfireShatter>();
                    section._rayfire.voronoi.amount = _rayfire.voronoi.amount;
                    section._rayfire.voronoi.centerBias = _rayfire.voronoi.centerBias;
                    section._rayfire.mode = _rayfire.mode;
                    section._rayfire.advanced.seed = _rayfire.advanced.seed;
                    section._rayfire.advanced.copyComponents = _rayfire.advanced.copyComponents;
                    section._breakAble = true;
                }
                else
                {
                    section._breakAble = false;
                }

                section._rockMaterial = _rockMaterial;
                section._meshBroken = true;
                NetworkManager.NetworkedObjects.Add(section.UID, section);

                Rigidbody rigidbody = section.GetComponent<Rigidbody>();
                rigidbody.AddExplosionForce(10, hitPosition, 10);
            }

        }

        using (DarkRiftWriter writer = DarkRiftWriter.Create())
        {
            writer.Write(UID);
            writer.Write(_health);
            if (_health <= 0)
            {
                writer.Write(hitPosition);

                // The seed should mean the amount in the list is the exact same
                // So we don't need to send a count to the client
                for (int i = 0; i < uids.Count; i++)
                {
                    writer.Write(uids[i]);
                }
            }

            NetworkManager.SendMessage(NetworkTags.HitNode, writer, SendMode.Reliable);
        }

        if (_health <= 0)
            Destroy(gameObject);
    }

    private void ClientDamageNode(DarkRiftReader reader)
    {
        _health = reader.ReadSingle();

        Debug.Log("Received Damage Node. New Heath = " + _health);

        if (_health <= 0)
        {
            Vector3 hitPosition = reader.ReadVector3();

            _rayfire.centerPosition = hitPosition;
            _rayfire.Fragment();
            _rayfire.gameObject.SetActive(false);

            for (int i = 0; i < _rayfire.fragmentsLast.Count; i++)
            {
                Node section = _rayfire.fragmentsLast[i].GetComponent<Node>();
                section.UID = reader.ReadUInt16();
                section._health = _startingHealth;
                section.Sync = Sync;

                MeshCollider meshCollider = _rayfire.fragmentsLast[i].GetComponent<MeshCollider>();
                if (meshCollider.bounds.size.magnitude <= 0.1)
                {
                    section._rayfire = _rayfire.fragmentsLast[i].AddComponent<RayfireShatter>();
                    section._rayfire.voronoi.amount = _rayfire.voronoi.amount;
                    section._rayfire.voronoi.centerBias = _rayfire.voronoi.centerBias;
                    section._rayfire.mode = _rayfire.mode;
                    section._rayfire.advanced.seed = _rayfire.advanced.seed;
                    section._rayfire.advanced.copyComponents = _rayfire.advanced.copyComponents;
                    section._breakAble = true;
                }
                else
                {
                    section._breakAble = false;
                }

                section._rockMaterial = _rockMaterial;
                section._meshBroken = true;
                NetworkManager.NetworkedObjects.Add(section.UID, section);
            }

            Destroy(gameObject);
        }
    }

    public override void InitilizeData(out Message message, out DarkRiftWriter writer)
    {
        writer = DarkRiftWriter.Create();

        writer.Write(Prefab.name);
        writer.Write(UID);
        writer.Write(transform.position);
        writer.Write(transform.rotation.eulerAngles);

        writer.Write(_health);
        writer.Write(transform.lossyScale);

        writer.Write(_meshBroken);
        if (_meshBroken)
        {
            MeshFilter meshFilter = GetComponent<MeshFilter>();
            Mesh mesh = meshFilter.mesh;
            writer.WriteMesh(mesh);
        }

        writer.Write(_breakAble);

        message = Message.Create((ushort)NetworkTags.SpawnPrefab, writer);
    }

    public override void Initialize(DarkRiftReader reader)
    {
        base.Initialize(reader);

        _health = reader.ReadSingle();
        transform.localScale = reader.ReadVector3();

        if (reader.ReadBoolean())
        {
            Mesh newMesh = reader.ReadMesh();
            MeshFilter meshFilter = GetComponent<MeshFilter>();
            meshFilter.mesh = newMesh;
            MeshCollider meshCollider = GetComponent<MeshCollider>();
            meshCollider.sharedMesh = newMesh;
        }

        _breakAble = reader.ReadBoolean();
        if (!_breakAble)
        {
            Destroy(_rayfire);
        }
    }

    private void OnDestroy()
    {
        NetworkManager.NetworkedObjects.Remove(UID);
        Debug.Log($"Removed Networked Object {UID}");
    }
}