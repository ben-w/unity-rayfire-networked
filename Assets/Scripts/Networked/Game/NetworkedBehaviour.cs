﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using DarkRift;
using Gather1.Server;

class NetworkedBehaviour : MonoBehaviour
{
    public enum SyncType { None, Transform }
    public ushort UID
    {
        get => _uid;
        set
        {
            if (_uid != 0)
            {
                Debug.LogWarning("Tried to change UID");
                return;
            }
            _uid = value;
        }
    }
    private ushort _uid;
    public GameObject Prefab;
    public Action<Message, DarkRiftReader> MessageReceived;

    public bool ClientAuth = false;
    public SyncType Sync = SyncType.None;
    [SerializeField] private float _threshold = 0.1f;
    private Vector3 _lastPosition;
    private Vector3 _lastRotation;
    private float _rotXDifferent;
    private float _rotYDifferent;
    private float _rotZDifferent;

    public virtual void OnEnable() => MessageReceived += NewMessage;

    public virtual void OnDisable() => MessageReceived -= NewMessage;

    public virtual void Initialize(DarkRiftReader reader)
    {
        _uid = reader.ReadUInt16();

        Vector3 position = new Vector3()
        {
            x = reader.ReadSingle(),
            y = reader.ReadSingle(),
            z = reader.ReadSingle()
        };
        Vector3 rotation = new Vector3()
        {
            x = reader.ReadSingle(),
            y = reader.ReadSingle(),
            z = reader.ReadSingle()
        };

        transform.position = position;
        transform.rotation = Quaternion.Euler(rotation);
        NetworkManager.NetworkedObjects.Add(_uid, this);
        Debug.Log($"Added {_uid} to Networked Objects");
    }

    public virtual void InitilizeData(out Message message, out DarkRiftWriter writer)
    {
        writer = DarkRiftWriter.Create();

        writer.Write(Prefab.name);
        writer.Write(UID);
        Vector3 pos = transform.position;
        writer.Write(pos.x);
        writer.Write(pos.y);
        writer.Write(pos.z);
        Vector3 rot = transform.rotation.eulerAngles;
        writer.Write(rot.x);
        writer.Write(rot.y);
        writer.Write(rot.z);

        message = Message.Create((ushort)NetworkTags.SpawnPrefab, writer);
    }

    public virtual void Update()
    {
        if ((!ClientAuth && !NetworkManager.IsServer) ||
            Sync == SyncType.None)
            return;
        SyncTransform();
    }

    private void SyncTransform()
    {
        _rotXDifferent = _lastRotation.x - transform.rotation.eulerAngles.x;
        _rotYDifferent = _lastRotation.y - transform.rotation.eulerAngles.y;
        _rotZDifferent = _lastRotation.z - transform.rotation.eulerAngles.z;

        if (_rotXDifferent < 0)
            _rotXDifferent = -_rotXDifferent;
        if (_rotYDifferent < 0)
            _rotYDifferent = -_rotYDifferent;
        if (_rotZDifferent < 0)
            _rotZDifferent = -_rotZDifferent;

        if (Vector3.Distance(_lastPosition, transform.position) > _threshold ||
            _rotXDifferent > _threshold ||
            _rotYDifferent > _threshold ||
            _rotZDifferent > _threshold)
        {
            _lastPosition = transform.position;
            _lastRotation = transform.rotation.eulerAngles;

            using (DarkRiftWriter writer = DarkRiftWriter.Create())
            {
                writer.Write(UID);

                writer.Write(_lastPosition.x);
                writer.Write(_lastPosition.y);
                writer.Write(_lastPosition.z);

                writer.Write(_lastRotation.x);
                writer.Write(_lastRotation.y);
                writer.Write(_lastRotation.z);

                NetworkManager.SendMessage(NetworkTags.MoveObject, writer, SendMode.Unreliable);
                Debug.Log("Sent MoveObject Message");
            }
        }
    }

    private void UpdateTransform(DarkRiftReader reader)
    {
        if (ClientAuth && !NetworkManager.IsServer)
            return;
        Debug.Log($"Received MoveObject Message");
        _lastPosition = reader.ReadVector3();
        _lastRotation = reader.ReadVector3();

        transform.position = _lastPosition;
        transform.rotation = Quaternion.Euler(_lastRotation);

        if (!NetworkManager.IsServer)
            return;

        using (DarkRiftWriter writer = DarkRiftWriter.Create())
        {
            writer.Write(UID);

            writer.Write(_lastPosition);
            writer.Write(_lastRotation);

            using (Message message = Message.Create((ushort)NetworkTags.MoveObject, writer))
            {
                NetworkManager.ServerSendMessageToAll(message, SendMode.Unreliable);
            }
        }
    }

    private void NewMessage(Message message, DarkRiftReader reader)
    {
        switch (message.Tag)
        {
            case (ushort)NetworkTags.MoveObject:
                UpdateTransform(reader);
                break;
        }
    }
}