﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DarkRift;
using DarkRift.Server;
using UnityEngine;

namespace Gather1.Server
{
    class MasterPlugin : Plugin
    {

        public static Action<object, ClientConnectedEventArgs> ClientConnected;
        public static Action<object, ClientDisconnectedEventArgs> ClientDisconnected;
        public override bool ThreadSafe => false;

        public override Version Version => new Version(1, 0, 0);

        public MasterPlugin(PluginLoadData pluginLoadData) : base(pluginLoadData)
        {
            ClientManager.ClientConnected += Connected;
            ClientManager.ClientDisconnected += Disconnected;
        }

        private void Disconnected(object sender, ClientDisconnectedEventArgs e)
        {
            Log("Client Disconnected");
            ClientDisconnected?.Invoke(sender, e);
        }

        private void Connected(object sender, ClientConnectedEventArgs e)
        {
            Log("Client Connected " + e.Client.ID);
            ClientConnected?.Invoke(sender, e);
        }

        public static void Log(object message) => Debug.Log(message);
        public static void LogWarning(object message) => Debug.LogWarning(message);
        public static void LogError(object message) => Debug.LogError(message);
    }
}
