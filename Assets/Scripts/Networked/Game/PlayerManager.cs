using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DarkRift;
using System;

class PlayerManager : NetworkedBehaviour
{
    public bool IsOwner = false;
    public ushort OwnerID;
    private float _xRotation = 0f;


    [Header(" Local Camera ")]
    [SerializeField] private float _mouseSensitivity;
    [SerializeField] private Transform _cameraPivot;
    private float _mouseX;
    private float _mouseY;

    [Header(" Local Movement ")]
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private Transform _groundCheck;
    [SerializeField] private float _movementSpeed;
    [SerializeField] private float _jumpHeight;
    private float _xMovement;
    private float _zMovement;
    private float _yMovement;
    private Vector3 _lastMovement;
    private bool _jumpKeyPressed;

    public void EnableCompoents()
    {
        NetworkManager.PlayerJoined?.Invoke(this);
        _cameraPivot.gameObject.SetActive(IsOwner);
        _rigidbody.isKinematic = !IsOwner;
        ClientAuth = IsOwner;

        if (IsOwner)
            Cursor.lockState = CursorLockMode.Locked;
    }

    public override void InitilizeData(out Message message, out DarkRiftWriter writer)
    {
        writer = DarkRiftWriter.Create();

        writer.Write(Prefab.name);
        writer.Write(UID);
        Vector3 pos = transform.position;
        writer.Write(pos.x);
        writer.Write(pos.y);
        writer.Write(pos.z);
        Vector3 rot = transform.rotation.eulerAngles;
        writer.Write(rot.x);
        writer.Write(rot.y);
        writer.Write(rot.z);

        writer.Write(OwnerID);
        Debug.Log("This is the correct one it called");
        message = Message.Create((ushort)NetworkTags.SpawnPrefab, writer);
    }

    public override void Initialize(DarkRiftReader reader)
    {
        base.Initialize(reader);
        OwnerID = reader.ReadUInt16();
        IsOwner = NetworkManager.Client.ID == OwnerID;
        gameObject.name = $"({OwnerID}) Player";

        EnableCompoents();
        NetworkManager.PlayersManagers.Add(OwnerID, this);
    }


    public override void Update()
    {
        base.Update();
        if (!IsOwner) return;
        Jumping();
        Movement();
        Looking();
        MouseLock();
    }

    private void MouseLock()
    {
        if (Cursor.lockState == CursorLockMode.Locked && Input.GetKeyDown(KeyCode.Escape))
            Cursor.lockState = CursorLockMode.None;
        else if (Input.GetMouseButtonDown(0) && Cursor.lockState == CursorLockMode.None)
            Cursor.lockState = CursorLockMode.Locked;
    }

    private void FixedUpdate()
    {
        if (!IsOwner) return;
        MovePlayer();
    }

    private void Movement()
    {
        _xMovement = Input.GetAxis("Horizontal");
        _zMovement = Input.GetAxis("Vertical");

        _lastMovement =
            transform.right * _xMovement +
            transform.forward * _zMovement +
            transform.up * _yMovement;
    }

    private void MovePlayer()
    {
        _rigidbody.velocity = new Vector3(
            _lastMovement.x * _movementSpeed,
            _rigidbody.velocity.y,
            _lastMovement.z * _movementSpeed);
    }

    private void Looking()
    {
        _mouseX = Input.GetAxis("Mouse X") * _mouseSensitivity * Time.deltaTime;
        _mouseY = Input.GetAxis("Mouse Y") * _mouseSensitivity * Time.deltaTime;

        _xRotation -= _mouseY;
        _xRotation = Mathf.Clamp(_xRotation, -90f, 90f);

        _cameraPivot.localRotation = Quaternion.Euler(_xRotation, 0f, 0f);

        transform.Rotate(Vector3.up * _mouseX);
    }

    private void Jumping()
    {
        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            _rigidbody.AddForce(transform.up * _jumpHeight, ForceMode.Impulse);
        }
    }

    private bool IsGrounded()
    {
        return Physics.Raycast(_groundCheck.position, -_groundCheck.up, 0.1f);
    }

    private void OnDestroy()
    {
        NetworkManager.PlayerDisconnected?.Invoke(this);
    }
}
