using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

class PlayerListItem : MonoBehaviour
{
    public string Name;
    public int Money;
    [SerializeField] private TextMeshProUGUI _text;

    public void Setup(string name, int money)
    {
        _text.text = $"{name} : {money}";
    }
}
