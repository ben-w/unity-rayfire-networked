using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DarkRift.Client.Unity;
using System.Net;
using UnityEngine.SceneManagement;
using DarkRift.Server.Unity;
using System.Collections.Specialized;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] private TMP_InputField _inputField;

    private void Start()
    {
        _inputField.text = NetworkManager.ConnectIP;
    }

    public void Connect()
    {
        Debug.Log("Connecting to " + _inputField.text);
        NetworkManager.ConnectIP = _inputField.text;
        SceneManager.LoadScene(1);
    }

    public void Host()
    {
        Debug.Log("Hosting");
        NetworkManager.IsServer = true;
        SceneManager.LoadScene(1);
    }
}
