using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class PlayerInstance : MonoBehaviour
{
    private const string _scriptName = "<b>PlayerInstance</b>";

    public ushort ID;
    public Transform Transform;
    private GameObject _playerObject;
    public NetworkedBehaviour PlayerManager;
}
