﻿public enum NetworkTags
{
    None,
    PlayerJoined,
    PlayerDisconnected,
    RequestPlayer,
    SpawnPlayer,
    SpawnPrefab,
    MoveObject,
    RequestSpawn,
    HitNodeRequest,
    HitNode,
    ToolSwitch,
    ToolHide
}