﻿using UnityEngine;
using DarkRift;

static class Utility
{
    public static void Write(this DarkRiftWriter writer, Vector2 vector2)
    {
        writer.Write(vector2.x);
        writer.Write(vector2.y);
    }

    public static Vector2 ReadVector2(this DarkRiftReader reader)
    {
        return new Vector2()
        {
            x = reader.ReadSingle(),
            y = reader.ReadSingle()
        };
    }

    public static void Write(this DarkRiftWriter writer, Vector2[] vector2s)
    {
        writer.Write(vector2s.Length);
        for (int i = 0; i < vector2s.Length; i++)
        {
            writer.Write(vector2s[i]);
        }
    }

    public static Vector2[] ReadVector2s(this DarkRiftReader reader)
    {
        int count = reader.ReadInt32();
        Vector2[] vector2s = new Vector2[count];
        for (int i = 0; i < count; i++)
        {
            vector2s[i] = reader.ReadVector2();
        }

        return vector2s;
    }

    public static void Write(this DarkRiftWriter writer, Vector3 vector3)
    {
        writer.Write(vector3.x);
        writer.Write(vector3.y);
        writer.Write(vector3.z);
    }

    public static Vector3 ReadVector3(this DarkRiftReader reader)
    {
        return new Vector3()
        {
            x = reader.ReadSingle(),
            y = reader.ReadSingle(),
            z = reader.ReadSingle()
        };
    }

    public static void Write(this DarkRiftWriter writer, Vector3[] vector3s)
    {
        writer.Write(vector3s.Length);
        for (int i = 0; i < vector3s.Length; i++)
        {
            writer.Write(vector3s[i]);
        }
    }

    public static Vector3[] ReadVector3s(this DarkRiftReader reader)
    {
        int count = reader.ReadInt32();
        Vector3[] vector3s = new Vector3[count];
        for (int i = 0; i < count; i++)
        {
            vector3s[i] = reader.ReadVector3();
        }

        return vector3s;
    }

    public static void Write(this DarkRiftWriter writer, int[] ints)
    {
        writer.Write(ints.Length);
        for (int i = 0; i < ints.Length; i++)
        {
            writer.Write(ints[i]);
        }
    }

    public static int[] ReadInts(this DarkRiftReader reader)
    {
        int count = reader.ReadInt32();
        int[] ints = new int[count];
        for (int i = 0; i < count; i++)
        {
            ints[i] = reader.ReadInt32();
        }

        return ints;
    }

    public static void WriteMesh(this DarkRiftWriter writer, Mesh mesh)
    {
        writer.Write(mesh.vertices);
        writer.Write(mesh.triangles);
        writer.Write(mesh.normals);
        writer.Write(mesh.uv);
    }

    public static Mesh ReadMesh(this DarkRiftReader reader)
    {
        Mesh mesh = new Mesh();
        mesh.vertices = reader.ReadVector3s();
        mesh.triangles = reader.ReadInts();
        mesh.normals = reader.ReadVector3s();
        mesh.uv = reader.ReadVector2s();
        return mesh;
    }
}